﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hero.BL
{
    public abstract class AbstractClass
    {
        public abstract int Health
        {
            get; set;

        }
        public abstract string Name
        {
            get; set;
        }
        public abstract void Heal();
        public abstract void Attack(AbstractClass enemy);
    }
}
