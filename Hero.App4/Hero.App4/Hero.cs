﻿using System;
using System.Collections.Generic;

namespace Hero.BL
{
    public class Hero : AbstractClass
    {
        private int health_;
        private string name_;


        public Hero(string name)
        {
            name_ = name;
            Health = 10;
        }

        public override string Name
        {
            get
            {
                return name_;
            }
            set
            {
                name_ = value;
            }
        }
        public override int Health
        {
            get
            {
                return health_;
            }
            set
            {
                health_ = value;
            }

        }

        public override void Attack(AbstractClass enemy)
        {
            enemy.Health--;
        }
        public override void Heal()
        {
            Health++;

        }
        public override string ToString()
        {
            return $"Name: {Name}\nHealth: {Health}\nExperience:";
        }
    }

}
