﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hero.BL
{
    public class Knight : AbstractClass
    {
        public Knight(string name)
        {
            name_ = Name;
            Health = 20;
        }
        private int health_;
        private string name_;


        public override int Health
        {
            get
            {
                return health_;
            }
            set
            {
                health_ = value;

            }
        }

        public override string Name
        {
            get
            {
                return name_;
            }
            set
            {
                name_ = value;
            }
        }

        public override void Attack(AbstractClass enemy)
        {
            enemy.Health--;
        }

        public override void Heal()
        {
            Health++;
        }

        public override string ToString()
        {
            return $"Name: {Name}\nHealth: {Health}\nExperience:";
        }
    }
}
