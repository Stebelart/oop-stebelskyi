﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hero.BL
{
    public class Vampire : AbstractClass, Interface
    {
        private int health_;
        private string name_;


        public Vampire(string name)
        {
            name_ = name;
            Health = 100000;
        }

        public override string Name
        {
            get
            {
                return name_;
            }
            set
            {
                name_ = value;
            }
        }
        public override int Health
        {
            get
            {
                return health_;
            }
            set
            {
                health_ = value;
            }

        }
        public override void Attack(AbstractClass enemy)
        {
            enemy.Health--;
        }
        public override void Heal()
        {
            Health++;

        }

        public void HyperAttack(AbstractClass enemy)
        {
            Health -= 100;
            enemy.Health -= 10;
        }


        public override string ToString()
        {
            return $"Name: {Name}\nHealth: {Health}\nExperience:";
        }
    }

}
