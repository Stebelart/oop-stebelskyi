﻿using Hero.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hero.App3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Write name of your hero: ");
            string name = Console.ReadLine();
            AbstractClass Tema = new Hero(name);
            AbstractClass Midav = new Knight(name);
            AbstractClass Sra = new Vampire(name);
            Console.WriteLine("Type any of specified commands for work:");
            bool work = true;
            while (work)
            {
                string option = Console.ReadLine();
                switch (option)
                {
                    case "attack":
                        Tema.Attack();
                        Console.WriteLine($"{Tema.Name} attacked. Strength reduced");
                        break;
                    case "heal":
                        Tema.Heal();
                        Console.WriteLine($"{Tema.Name} healed ");
                        break;
                    case "stats":
                        Console.WriteLine(Tema);
                        break;
                    case "exit":
                        work = false;
                        break;
                    default:
                        Console.WriteLine("Error");

                        break;
                }

            }

            Console.ReadKey();
        }


    }
}
