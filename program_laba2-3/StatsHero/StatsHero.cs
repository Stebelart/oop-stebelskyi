﻿using System;

namespace stats_hero
{
    public class Hero
    {
        private int _health;
        private int _mana;
        private int _damage;

        public int Health
        {
            get { return _health; }
            set { _health = value; }
        }
        public int Mana
        {
            get { return _mana; }
            set { _mana = value; }
        }
        public int Damage
        {
            get { return _damage; }
            set { _damage = value; }
        }

        public void Attak(Hero hero)
        {
            hero.Health = hero.Health - Damage;
        }
        public void Spell(Hero hero)
        {
            hero.Health = hero.Health - Damage * 2;
        }
        public void ManaCost(Hero hero)
        {
            hero.Mana = hero.Mana - 3;
        }
        public void DamageHealth()
        {
            Health--;
        }
        public void HealthHeal()
        {
            Health++;
        }
        public void ManaCosts()
        {
            Mana--;
        }
        public void ManaRegen()
        {
            Mana++;
        }
        public void DamagePlus()
        {
            Damage++;
        }
        public void DamageMinus()
        {
            Damage--;
        }

        public void CheckDead()
        {
            if (Health <= 0)
            {
                Console.WriteLine($"You dead...");
            }
        }
        public void Enemy_1()                              //laba2
        {
            Health -= 2;
            if (Health > 0)
                Console.WriteLine($"You won First Enemy!");
            else
                Console.WriteLine($"You dead...");
        }

        public override string ToString()
        {
            return $"Health: {Health} Mana: {Mana} Damage:{Damage}";
        }
    }
    public class Wizard : Hero                       //laba3
    {
        private int _range;

        public int Range
        {
            get { return _range; }
            set { _range = value; }
        }
        public void FireBall(Hero hero)
        {
            if (Range > 0)
                hero.Health = hero.Health - Damage * 4;
        }
    }
}