﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stats_hero
{
    class Program
    {
        static void Main(string[] args)
        {
            Hero userhero = new Hero
            {
                Health = 10,
                Mana = 10,
                Damage = 10,
            };

            Hero enemy1hero = new Hero
            {
                Health = 20,
                Damage = 2,
            };

            Hero enemy2hero = new Hero
            {
                Health = 30,
                Damage = 4,
            };

            bool WhileStatus = true;

            while (WhileStatus == true)
            {
                Console.WriteLine(userhero.ToString());

                StringBuilder MainChoiceMenu = new StringBuilder();
                MainChoiceMenu.AppendLine("Choice action:");
                MainChoiceMenu.AppendLine("1.Damage Heath hero.");
                MainChoiceMenu.AppendLine("2.Use mana.");
                MainChoiceMenu.AppendLine("3.Increase damage.");
                MainChoiceMenu.AppendLine("4.Healing health.");
                MainChoiceMenu.AppendLine("5.Regen mana.");
                MainChoiceMenu.AppendLine("6.Decrease damage.");
                MainChoiceMenu.AppendLine($"7.Attack FirstEnemy. Health:{enemy1hero.Health}, Damage:{enemy1hero.Damage}, ");
                MainChoiceMenu.AppendLine($"8.Attack SecondEnemy. Health:{enemy2hero.Health}, Damage:{enemy2hero.Damage},");
                Console.Write(MainChoiceMenu);


                Console.WriteLine($"Enter number: ");
                int num = Convert.ToInt32(Console.ReadLine());
                switch (num)
                {
                    case 1:
                        userhero.DamageHealth();
                        Console.WriteLine(userhero.ToString());
                        break;
                    case 2:
                        userhero.ManaCosts();
                        Console.WriteLine(userhero.ToString());
                        break;
                    case 3:
                        userhero.DamagePlus();
                        Console.WriteLine(userhero.ToString());
                        break;
                    case 4:
                        userhero.HealthHeal();
                        Console.WriteLine(userhero.ToString());
                        break;
                    case 5:
                        userhero.ManaRegen();
                        Console.WriteLine(userhero.ToString());
                        break;
                    case 6:
                        userhero.DamageMinus();
                        Console.WriteLine(userhero.ToString());
                        break;
                    case 7:
                        Console.WriteLine($"How you want attack enemy? 1 - Standart 2 - Spell");
                        int i = Convert.ToInt32(Console.ReadLine());
                        if (i == 1)
                        {
                            userhero.Attak(enemy1hero);
                            enemy1hero.Attak(userhero);
                            Console.WriteLine(userhero.ToString());
                            Console.WriteLine(enemy1hero.ToString());
                        }
                        else if (i == 2)
                        {
                            userhero.Spell(enemy1hero);
                            userhero.ManaCost(userhero);

                        }
                        if (enemy1hero.Health <= 0)
                        {
                            Console.WriteLine($"Enemy hero is dead!");
                            Console.ReadKey();
                        }
                        break;
                    case 8:
                        Console.WriteLine($"How you want attack enemy? 1 - Standart 2 - Spell");
                        int x = Convert.ToInt32(Console.ReadLine());
                        if (x == 1)
                        {
                            userhero.Attak(enemy2hero);
                            enemy2hero.Attak(userhero);
                        }
                        else if (x == 2)
                        {
                            userhero.Spell(enemy2hero);
                            userhero.ManaCost(userhero);

                        };
                        if (enemy2hero.Health <= 0)
                        {
                            Console.WriteLine($"Enemy hero is dead!");
                            Console.ReadKey();
                        }
                        break;
                }
                Console.Clear();

                if (userhero.Health <= 0)
                {
                    WhileStatus = false;
                    userhero.CheckDead();
                    Console.WriteLine(userhero.ToString());
                }
            }
        }
    }
}